<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center" style="padding-top:20px;">
            {{-- <i class="icon-c-logo"> <img src="{{ asset('Purple/assets/images/Logo.png') }}" height="42" /> </i>
            <span><img src="{{ asset('Purple/assets/images/Logo-SIP-RPM.png') }}" height="55" /></span> --}}
            <a href="#" class="logo">
                <i class="icon-c-logo"> <img src="{{ asset('Purple/assets/images/Logo.png') }}" height="42" /> </i>
                <span><img src="{{ asset('Purple/assets/images/Logo.png') }}" height="55" /></span>
            </a>
            {{-- <a href="index.html" class="logo"><i class="icon-magnet icon-c-logo"></i><span>Ub<i class="md md-album"></i>ld</span></a> --}}
            <!-- Image Logo here -->
            <!--<a href="index.html" class="logo">-->
            <!--<i class="icon-c-logo"> <img src="assets/images/logo_sm.png" height="42"/> </i>-->
            <!--<span><img src="assets/images/logo_light.png" height="20"/></span>-->
            <!--</a>-->
        </div>
    </div>
    <div style="background-color: 	#4169E1;height: 20px;"></div>
    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation" style="background-color=#fff">
        <div class="container">
            <div class="">
                <div class="pull-left">
                    <button class="button-menu-mobile open-left waves-effect waves-light" style="color: #777;">
                        <i class="md md-menu"></i>
                    </button>
                    <span class="clearfix"></span>
                </div>

                {{-- <ul class="nav navbar-nav hidden-xs">
                    <li><a href="#" class="waves-effect waves-light">{{Auth::user()->name}}</a></li>
                </ul> --}}
                <ul class="nav navbar-nav hidden-xs">

                    {{-- <li><a href="#" class="waves-effect waves-light">{{Auth::user()->name}}</a></li> --}}
                    {{-- <li><a href="#" class="waves-effect waves-light">{{Auth::user()->email}}</a></li> --}}
                    {{-- <li><a href="#" class="waves-effect waves-light">AS OFF : {{Session::get('ASOff')}}</a></li>
                    --}}

                    <li><a href="#" class="waves-effect waves-light">Business Date : 2020-10-09</a></li>

                    {{-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
                            role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span
                                class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </li> --}}
                </ul>

                <ul class="nav navbar-nav navbar-right pull-right">


                    <li class="hidden-xs">
                        <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i
                                class="icon-size-fullscreen"></i></a>
                    </li>
                    {{-- <li class="dropdown top-menu-item-xs">
                        <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown"
                            aria-expanded="true"><img src="{{ asset('Purple/assets/images/users/avatar-11.jpg') }}"
                    alt="user-img" class="img-circle"> </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('gantipassword') }}"><i class="ti-settings m-r-10 text-custom"></i>
                                Change Password</a></li>
                        <li><a href="{{ route('GantiSessionBranch') }}"><i class="fa fa-cubes m-r-10 text-custom"></i>
                                {{Session::get('RegionName')}}</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-lock m-r-10 text-custom"></i> Lock
                                screen</a></li>
                        <li class="divider"></li>
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                                <i class="ti-power-off m-r-10 text-danger"></i>
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                    </li> --}}
                    <li class="dropdown top-menu-item-xs">
                        <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown"
                            aria-expanded="true"><img src="{{ asset('Purple/assets/images/users/avatar-1.jpg')}}"
                                alt="user-img" class="img-circle"> &nbsp;</a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0)"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                            {{-- <li><a href="{{ route('gantipassword') }}"><i
                                class="ti-settings m-r-10 text-custom"></i>
                            Change
                            Password</a>
                    </li> --}}
                    <li class="divider"></li>
                    <li>
                        <a href=""><i class="ti-power-off m-r-10 text-danger"></i>
                            Logout</a>

                        {{-- <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i class="ti-power-off m-r-10 text-danger"></i>
                        {{ __('Logout') }}
                        </a> --}}
                        {{-- <form id="logout-form" action="{{ route('logout') }}" method="POST"
                        style="display: none;">
                        @csrf
                        </form> --}}
                    </li>
                </ul>
                </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
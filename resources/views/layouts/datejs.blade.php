<script src="{{ asset('Purple/assets/plugins/moment/moment.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

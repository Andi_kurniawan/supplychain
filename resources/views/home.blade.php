@extends('layouts.app')

@section('content')

@include('layouts.datatablecss')



<link rel="stylesheet" href="{{ asset('Purple/assets/plugins/morris/morris.css') }}">
<link href="{{ asset('Purple/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('Purple/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />

<style type="text/css">
    .nvd3 text {
        font: 400 9px Arial;
    }

    .btn-primary,
    .btn-success,
    .btn-default,
    .btn-info,
    .btn-warning,
    .btn-danger,
    .btn-inverse,
    .btn-purple,
    .btn-pink {
        color: #000000 !important;
        border: 1px solid #ffffff !important;
    }

    .panel .panel-body {
        padding: 14px 0px 0px 0px;
    }

    .nav.nav-tabs+.tab-content {
        margin-bottom: 0px;
        padding: 10px;
        min-height: 505px;
    }
</style>

<div class="content">
    <div class="container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                {{-- <div class="pull-right m-t-10">

                    <button type="button" style="border: 1px solid #053aef !important;"
                        class="btn btn-default btn-custom btn-rounded waves-effect waves-light right-bar-toggle"
                        aria-expanded="false">
                        <span style="font-weight: normal;">MTD</span>
                    </button>

                    <button type="button" style="background: #053aef;border: 1px solid #053aef !important;"
                        class="btn btn-default btn-rounded waves-effect waves-light right-bar-toggle"
                        aria-expanded="false">
                        <span style="font-weight: normal; color:#ffffff">YTD</span>
                    </button>

                </div> --}}

                <h4 class="page-title"></h4>
                <p class="text-muted page-title-alt"></p>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-default dropdown-toggle waves-effect" data-toggle="dropdown"
                        aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                    <ul class="dropdown-menu drop-menu-right" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
                <h4 class="page-title">Summary</h4>
                <p class="text-muted page-title-alt">Partner Management Highlight</p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="widget-panel widget-style-2 bg-white">
                    <i class="md md-attach-money text-primary"></i>
                    <h2 class="m-0 text-dark counter font-600">50568</h2>
                    <div class="text-muted m-t-5">Top Brand</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="widget-panel widget-style-2 bg-white">
                    <i class="md md-add-shopping-cart text-pink"></i>
                    <h2 class="m-0 text-dark counter font-600">1256</h2>
                    <div class="text-muted m-t-5">Pricing Growth</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="widget-panel widget-style-2 bg-white">
                    <i class="md md-store-mall-directory text-info"></i>
                    <h2 class="m-0 text-dark counter font-600">18</h2>
                    <div class="text-muted m-t-5">Success Rate</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="widget-panel widget-style-2 bg-white">
                    <i class="md md-account-child text-custom"></i>
                    <h2 class="m-0 text-dark counter font-600">8564</h2>
                    <div class="text-muted m-t-5">Sold Amount</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Sales Achievment</b></h4>
                    {{-- <p class="text-muted m-b-30 font-13">
                        Example of Label placement chart.
                    </p> --}}

                    <div id="gauge-chart" class="ct-chart ct-golden-section"></div>
                </div>
            </div>

            <!-- Donut Chart -->
            <div class="col-lg-6">

                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Sales Distribution by Unit</b></h4>
                    {{-- <p class="text-muted m-b-30 font-13">
                        Example of Horizontal bar chart.
                    </p> --}}

                    <div id="pie-chart" class="ct-chart ct-golden-section"></div>
                </div>
                <!-- /Portlet -->
            </div>
        </div>
        <!-- End row-->

        <div class="row">
            <div class="col-lg-12">
                <div class="card-box">
                    <h4 class="text-dark header-title m-t-0">Trend Of Selling</h4>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="text-center">
                                <ul class="list-inline chart-detail-list">
                                    <li>
                                        <h5><i class="fa fa-circle m-r-5" style="color: #36404a;"></i>Mobil</h5>
                                    </li>
                                    <li>
                                        <h5><i class="fa fa-circle m-r-5" style="color: #5d9cec;"></i>Motor</h5>
                                    </li>
                                    <li>
                                        <h5><i class="fa fa-circle m-r-5" style="color: #bbbbbb;"></i>Morthgage</h5>
                                    </li>
                                </ul>
                            </div>

                            <div id="morris-area-with-dotted" style="height: 300px;"></div>

                        </div>



                        <div class="col-md-4">

                            <p class="font-600">Toyota Avanza <span class="text-primary pull-right">80%</span></p>
                            <div class="progress m-b-30">
                                <div class="progress-bar progress-bar-primary progress-animated wow animated"
                                    role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 80%">
                                </div><!-- /.progress-bar .progress-bar-danger -->
                            </div><!-- /.progress .no-rounded -->

                            <p class="font-600">Honda Supra X <span class="text-pink pull-right">50%</span></p>
                            <div class="progress m-b-30">
                                <div class="progress-bar progress-bar-pink progress-animated wow animated"
                                    role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 50%">
                                </div><!-- /.progress-bar .progress-bar-pink -->
                            </div><!-- /.progress .no-rounded -->

                            <p class="font-600">Yamaha Aerox 155 ABS <span class="text-info pull-right">70%</span></p>
                            <div class="progress m-b-30">
                                <div class="progress-bar progress-bar-info progress-animated wow animated"
                                    role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 70%">
                                </div><!-- /.progress-bar .progress-bar-info -->
                            </div><!-- /.progress .no-rounded -->

                            <p class="font-600">Suzuki XL7<span class="text-warning pull-right">65%</span>
                            </p>
                            <div class="progress m-b-30">
                                <div class="progress-bar progress-bar-warning progress-animated wow animated"
                                    role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 65%">
                                </div><!-- /.progress-bar .progress-bar-warning -->
                            </div><!-- /.progress .no-rounded -->

                            <p class="font-600">Yamaha MIO <span class="text-success pull-right">40%</span>
                            </p>
                            <div class="progress m-b-30">
                                <div class="progress-bar progress-bar-success progress-animated wow animated"
                                    role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                    style="width: 40%">
                                </div><!-- /.progress-bar .progress-bar-success -->
                            </div><!-- /.progress .no-rounded -->


                        </div>



                    </div>

                    <!-- end row -->

                </div>

            </div>



        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title"><b>Margin Fluctuation</b></h4>

                    <div class="line-chart">
                        <svg style="height:400px;width:100%"></svg>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="widget-bg-color-icon card-box">
                    <div class="bg-icon bg-icon-info pull-left">
                        <i class="md md-attach-money text-info"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><b class="counter">1,000</b></h3>
                        <p class="text-muted">Unit Order</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="widget-bg-color-icon card-box">
                    <div class="bg-icon bg-icon-custom pull-left">
                        <i class="md md-add-shopping-cart text-custom"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><b class="counter">750</b></h3>
                        <p class="text-muted">Unit Selling</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-md-6 col-lg-3">
                <div class="widget-bg-color-icon card-box">
                    <div class="bg-icon bg-icon-custom pull-left">
                        <i class="md md-remove-red-eye text-custom"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><b class="counter">64 <small>mio</small></b></h3>
                        <p class="text-muted">Amount Selling</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="widget-bg-color-icon card-box">
                    <div class="bg-icon bg-icon-info pull-left">
                        <i class="md md-equalizer text-info"></i>
                    </div>
                    <div class="text-right">
                        <h3 class="text-dark"><b class="counter">3.05</b>%</h3>
                        <p class="text-muted">Margin Amount</p>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>


        <!-- end row -->

    </div>

</div>

<!-- /Portlet -->
<!-- End row-->


<!-- content -->

<footer class="footer text-right">
    © 2016. All rights reserved.
</footer>

</div>


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->
<div class="side-bar right-bar nicescroll">
    <h4 class="text-center">Chat</h4>
    <div class="contact-list nicescroll">
        <ul class="list-group contacts-list">
            <li class="list-group-item">
                <a href="#">
                    <div class="avatar">
                        <img src="assets/images/users/avatar-1.jpg" alt="">
                    </div>
                    <span class="name">Chadengle</span>
                    <i class="fa fa-circle online"></i>
                </a>
                <span class="clearfix"></span>
            </li>
            <li class="list-group-item">
                <a href="#">
                    <div class="avatar">
                        <img src="assets/images/users/avatar-2.jpg" alt="">
                    </div>
                    <span class="name">Tomaslau</span>
                    <i class="fa fa-circle online"></i>
                </a>
                <span class="clearfix"></span>
            </li>
            <li class="list-group-item">
                <a href="#">
                    <div class="avatar">
                        <img src="assets/images/users/avatar-3.jpg" alt="">
                    </div>
                    <span class="name">Stillnotdavid</span>
                    <i class="fa fa-circle online"></i>
                </a>
                <span class="clearfix"></span>
            </li>
            <li class="list-group-item">
                <a href="#">
                    <div class="avatar">
                        <img src="assets/images/users/avatar-4.jpg" alt="">
                    </div>
                    <span class="name">Kurafire</span>
                    <i class="fa fa-circle online"></i>
                </a>
                <span class="clearfix"></span>
            </li>
            <li class="list-group-item">
                <a href="#">
                    <div class="avatar">
                        <img src="assets/images/users/avatar-5.jpg" alt="">
                    </div>
                    <span class="name">Shahedk</span>
                    <i class="fa fa-circle away"></i>
                </a>
                <span class="clearfix"></span>
            </li>
            <li class="list-group-item">
                <a href="#">
                    <div class="avatar">
                        <img src="assets/images/users/avatar-6.jpg" alt="">
                    </div>
                    <span class="name">Adhamdannaway</span>
                    <i class="fa fa-circle away"></i>
                </a>
                <span class="clearfix"></span>
            </li>
            <li class="list-group-item">
                <a href="#">
                    <div class="avatar">
                        <img src="assets/images/users/avatar-7.jpg" alt="">
                    </div>
                    <span class="name">Ok</span>
                    <i class="fa fa-circle away"></i>
                </a>
                <span class="clearfix"></span>
            </li>
            <li class="list-group-item">
                <a href="#">
                    <div class="avatar">
                        <img src="assets/images/users/avatar-8.jpg" alt="">
                    </div>
                    <span class="name">Arashasghari</span>
                    <i class="fa fa-circle offline"></i>
                </a>
                <span class="clearfix"></span>
            </li>
            <li class="list-group-item">
                <a href="#">
                    <div class="avatar">
                        <img src="assets/images/users/avatar-9.jpg" alt="">
                    </div>
                    <span class="name">Joshaustin</span>
                    <i class="fa fa-circle offline"></i>
                </a>
                <span class="clearfix"></span>
            </li>
            <li class="list-group-item">
                <a href="#">
                    <div class="avatar">
                        <img src="assets/images/users/avatar-10.jpg" alt="">
                    </div>
                    <span class="name">Sortino</span>
                    <i class="fa fa-circle offline"></i>
                </a>
                <span class="clearfix"></span>
            </li>
        </ul>
    </div>
</div>
<!-- /Right-bar -->



</div> <!-- container -->


{{-- <script type=" text/javascript" src="{{ asset('js/app.js') }}">
</script> --}}

@include('layouts.js')


<!-- jQuery  -->
<script src="{{ asset('Purple/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/detect.js') }}"></script>
<script src="{{ asset('Purple/assets/js/fastclick.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('Purple/assets/js/waves.js') }}"></script>
<script src="{{ asset('Purple/assets/js/wow.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.scrollTo.min.js') }}"></script>


<script src="{{ asset('Purple/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.app.js') }}"></script>

<!--Morris Chart-->
<script src="{{ asset('Purple/assets/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/raphael/raphael-min.js') }}"></script>
<script src="{{ asset('Purple/assets/pages/morris.init.js') }}"></script>




<!-- jQuery  -->
<script src="{{ asset('Purple/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/detect.js') }}"></script>
<script src="{{ asset('Purple/assets/js/fastclick.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('Purple/assets/js/waves.js') }}"></script>
<script src="{{ asset('Purple/assets/js/wow.min.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.scrollTo.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/peity/jquery.peity.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>


<script src="{{ asset('Purple/assets/plugins/d3/d3.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/nvd3/nv.d3.min.js') }}"></script>

<script src="{{ asset('Purple/assets/pages/jquery.nvd3.init.js') }}"></script>

<script src="{{ asset('Purple/assets/pages/jquery.dashboard_3.js') }}"></script>

<script src="{{ asset('Purple/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.app.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/moment/moment.js') }}"></script>


{{-- <script src="{{ asset('Purple/assets/plugins/morris/morris.min.js') }}"></script> --}}
<script src="{{ asset('Purple/assets/plugins/raphael/raphael-min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>

<!-- Todojs  -->
<script src="{{ asset('Purple/assets/pages/jquery.todo.js') }}"></script>
<!--Chartist Chart-->
<script src="{{ asset('Purple/assets/plugins/chartist/js/chartist.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/chartist/js/chartist-plugin-tooltip.min.js') }}"></script>
<script src="{{ asset('Purple/assets/pages/jquery.chartist.init.js') }}"></script>



<script src="{{ asset('Purple/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.app.js') }}"></script>

<script src="{{ asset('Purple/assets/pages/jquery.dashboard_2.js') }}"></script>



<script src="{{ asset('Purple/assets/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>


<!-- Counterup  -->
<script src="{{ asset('Purple/assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/counterup/jquery.counterup.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/raphael/raphael-min.js') }}"></script>


<script src="{{ asset('Purple/assets/plugins/jquery-knob/jquery.knob.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/d3/d3.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/nvd3/nv.d3.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/jquery-circliful/js/jquery.circliful.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript">

</script>

<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.time.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.selection.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.orderBars.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/flot-chart/jquery.flot.crosshair.js') }}"></script>
<script src="{{ asset('Purple/assets/pages/jquery.flot.init.js') }}"></script>

@include('layouts.datatablejs')

@endsection
@extends('layouts.app')

@section('content')

@include('layouts.datatablecss')



<link rel="stylesheet" href="{{ asset('Purple/assets/plugins/morris/morris.css') }}">
<link href="{{ asset('Purple/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('Purple/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/core.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/components.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('Purple/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />

<style type="text/css">
    .nvd3 text {
        font: 400 9px Arial;
    }

    .btn-primary,
    .btn-success,
    .btn-default,
    .btn-info,
    .btn-warning,
    .btn-danger,
    .btn-inverse,
    .btn-purple,
    .btn-pink {
        color: #000000 !important;
        border: 1px solid #ffffff !important;
    }

    .panel .panel-body {
        padding: 14px 0px 0px 0px;
    }

    .nav.nav-tabs+.tab-content {
        margin-bottom: 0px;
        padding: 10px;
        min-height: 505px;
    }
</style>

<div class="content">
    <div class="container">

        <div class="row">
            <div class="col-md-6" style="">
                <img src="{{ asset('Frame1.png') }}" alt="">

            </div>
            <div class="col-md-6">

            </div>
        </div>
        <!-- Page-Title -->
        {{-- <div class="row">
            <div class="col-sm-12">

                <h4 class="page-title"></h4>
                <p class="text-muted page-title-alt"></p>
            </div>
        </div>

        <!-- Start content -->

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light"
                        data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i
                                class="fa fa-cog"></i></span></button>
                    <ul class="dropdown-menu drop-menu-right" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>

                <h4 class="page-title">Order</h4>
                <p class="text-muted page-title-alt">Welcome to Ubold admin panel !</p>
            </div>
        </div> --}}
        <footer class="footer text-right">
            © 2016. All rights reserved.
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->



</div> <!-- container -->
</div>

{{-- <script type=" text/javascript" src="{{ asset('js/app.js') }}">
</script> --}}

@include('layouts.js')

<script src="{{ asset('Purple/assets/plugins/moment/moment.js') }}"></script>


<script src="{{ asset('Purple/assets/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/raphael/raphael-min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>

<!-- Todojs  -->
<script src="{{ asset('Purple/assets/pages/jquery.todo.js') }}"></script>
{{-- 
<!-- chatjs  -->
<script src={{ asset('Purple/assets/pages/jquery.chat.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/peity/jquery.peity.min.js') }}"></script>

<script src="{{ asset('Purple/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('Purple/assets/js/jquery.app.js') }}"></script> --}}

<script src="{{ asset('Purple/assets/pages/jquery.dashboard_2.js') }}"></script>





<script src="{{ asset('Purple/assets/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/jquery-sparkline/jquery.sparkline.min.js') }}"></script>


<!-- Counterup  -->
<script src="{{ asset('Purple/assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/counterup/jquery.counterup.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('Purple/assets/plugins/raphael/raphael-min.js') }}"></script>


<script src="{{ asset('Purple/assets/plugins/jquery-knob/jquery.knob.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/d3/d3.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/nvd3/nv.d3.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/jquery-circliful/js/jquery.circliful.min.js') }}"></script>

<script src="{{ asset('Purple/assets/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript">

</script>

@include('layouts.datatablejs')

@endsection
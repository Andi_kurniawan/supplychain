<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Modul\ForgotPasswordController;
use App\Http\Controllers\Api\Modul\PagesController;
use App\Http\Controllers\Api\Modul\OrderController;
use App\Http\Controllers\Api\Modul\InvoiceController;
use App\Http\Controllers\Api\Modul\ReportController;
use App\Http\Controllers\Api\Modul\DashboardController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['prefix' => 'supplychain'], function () {
    Route::group(['prefix' => 'auth'], function () {

        Route::post('login', [LoginController::class, 'login']);
        Route::get('logout', 'AuthController@logout')->middleware('auth:api');
    });

    Route::group(['prefix' => 'ForgotPassword'], function () {
        Route::post('', [ForgotPasswordController::class, 'getToken_Forgotpassword']);
        Route::post('/Validate_Token_ForgotPassword', [ForgotPasswordController::class, 'ValidateToken_Forgotpassword']);
        Route::post('/ChangePassword', [ForgotPasswordController::class, 'changepassword']);
    });

    Route::post('/ContactUs', [PagesController::class, 'contactus']);

    Route::get('/YearMonth', [PagesController::class, 'yearmonth']);

    //admin
    Route::get('/getContactUs', [PagesController::class, 'getcontactus']);





    Route::group(['middleware' => 'auth:api'], function () {

        Route::post('/ChangePassword', [LoginController::class, 'changepassword']);
        Route::group(['prefix' => 'Order'], function () {
            Route::post('/Api', [OrderController::class, 'orderapi']);
            Route::post('/Manualupload', [OrderController::class, 'manualorder']);
            Route::post('/TrackingStatusOrder', [OrderController::class, 'trackingorder']);
            Route::post('/TrackingStatusOrder_Detail', [OrderController::class, 'trackingorderdetail']);
            Route::post('/sendEmail', [OrderController::class, 'email']);
            Route::post('/UploadDocument', [OrderController::class, 'uploaddocument']);
            Route::post('/NotificationOrder', [OrderController::class, 'notification']);
            Route::post('/OrderImages', [OrderController::class, 'OrderImages']);
        });

        Route::group(['prefix' => 'Invoice'], function () {
            Route::post('', [InvoiceController::class, 'invoice']);
            Route::post('/ReportInvoice', [InvoiceController::class, 'reportinvoice']);
            Route::post('/BuktiSetor', [InvoiceController::class, 'buktisetor']);
        });

        Route::group(['prefix' => 'Transaction'], function () {
            Route::post('', [InvoiceController::class, 'invoice']);
        });

        Route::group(['prefix' => 'Report'], function () {
            Route::post('ReportSelling', [ReportController::class, 'ReportSelling']);
            Route::post('ReportInvoice', [ReportController::class, 'ReportInvoice']);
            Route::post('ReportBuktiSetor', [ReportController::class, 'ReportBuktiSetor']);
        });

        Route::group(['prefix' => 'Dashboard'], function () {
            Route::post('/Summary', [DashboardController::class, 'summary']);
            Route::post('/UnitDistribution', [DashboardController::class, 'unitdistribution']);
            Route::post('/SalesTrend', [DashboardController::class, 'salestrend']);
            Route::post('/MarginTrend', [DashboardController::class, 'margintrend']);
            Route::post('/SummaryOrder', [DashboardController::class, 'summaryorder']);
            Route::post('/RecentOrder', [DashboardController::class, 'recentorder']);
            Route::post('/RecentSelling', [DashboardController::class, 'recentselling']);
            Route::post('/OrderProgress', [DashboardController::class, 'OrderProgress']);
            Route::post('/SalesTrend2', [DashboardController::class, 'salestrend2']);
            Route::post('/MarginTrend2', [DashboardController::class, 'margintrend2']);
        });
    });
});

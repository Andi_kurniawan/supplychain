<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // use HasFactory;
    public $table = 'Order';
    protected $fillable = [
        'OrderNo', 'PartnerID', 'PickupLocation', 'AgreementNo', 'Brand', 'AssetCode', 'AssetDescription', 'DocumentNo', 'TaxNoticed_No', 'LastAnnual_TaxNoticed_Billed', '5Year_ExpiredDate_TaxNotice', 'AssetCategory', 'ManufacturingYear', 'LicensePlate', 'Colour', 'EngineNo', 'ChassisNo', 'Cylinder', 'SecondaryKey', 'InventoryDate', 'InventoryAmount', 'AssetCondition', 'ProposedAmount', 'Proposed_Pickup_Date', 'Partner_InCharge', 'Partner_InCharge_MobilePhoneNo', 'OrderDate', 'Notes', 'SentDate', 'OrderBy', 'Status'
    ];
}

<?php

namespace App\Http\Controllers\Api\Modul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class TransactionController extends Controller
{
    public function transaction()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $validator = Validator::make($request->all(), [
            'PartnerID'     => 'required|string',
            'OrderNo'       => 'required|string',
            'AssetType'     => 'required|string',
            'LicensePlate'  => 'required|string',
            'Buyer_IDNo'    => 'required|string',
            'Buyer_Name'    => 'required|string',
            'BottomPrices'  => 'required|string',
            'SellingAmount' => 'required|string',
            'SellingDate'   => 'required|string',
            'Wanprestasi'   => 'required|string',
            'Notes'         =>'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['error' => $validator->errors()->all(), 'authUser' => false, 'code' => '422'],
                422
            );
        }

        if ($request) {
            // get request
            $PartnerID                    = $request['PartnerID'];
            $OrderNo                      = $request['OrderNo'];
            $AssetType                    = $request['AssetType'];
            $LicensePlate                 = $request['LicensePlate'];
            $Buyer_IDNo                   = $request['Buyer_IDNo'];
            $Buyer_Name                   = $request['Buyer_Name'];
            $BottomPrices                 = $request['BottomPrices'];
            $SellingAmount                = $request['SellingAmount'];
            $SellingDate                  = $request['SellingDate'];
            $Wanprestasi                  = $request['Wanprestasi'];
            $Notes                        = $request['Notes'];

            DB::Select("Call supplychain.spc_selling_transaction('$PartnerID','$AssetType','$OrderNo',$LicensePlate','$Buyer_IDNo','$Buyer_Name','$BottomPrices','$SellingAmount','$SellingDate','$Wanprestasi','$Notes')");
        }

        return response()->json(
            ['isValid' => true, 'ResponseCode' => '200', 'ResponseDescription' => 'Data Berhasil Disimpan'],
            200
        );
    }
}

<?php

namespace App\Http\Controllers\Api\Modul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Validator;
use App\Imports\OrderImport;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use App\Mail\NotifyMail;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\Filesystem;

class OrderController extends Controller
{
    public function orderapi(request $request)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        $date    = Carbon::now()->format('Y-m-d H:i:s');
        try {
            $data = DB::select("SELECT  * FROM `supplychain`.`Order` WHERE AgreementNo = '" . $request['AgreementNo'] . "'");
            if (count($data) < 1  and $request['AgreementNo'] != '') {

                $PartnerID = $request['PartnerID'];
                $PickupLocation = $request['PickupLocation'];
                $AgreementNo = $request['AgreementNo'];
                // $AssetID = $request['AssetID'];
                $AssetCode = $request['AssetCode'];
                $AssetDescription = $request['AssetDescription'];
                $DocumentNo = $request['DocumentNo'];
                $TaxNoticed_No = $request['TaxNoticed_No'];
                $LastAnnual_TaxNoticed_Billed = $request['LastAnnual_TaxNoticed_Billed'];
                $Year_ExpiredDate_TaxNotice = $request['5Year_ExpiredDate_TaxNotice'];
                $AssetCategory = $request['AssetCategory'];
                $ManufacturingYear = $request['ManufacturingYear'];
                $LicensePlate = $request['LicensePlate'];
                $Colour = $request['Colour'];
                $EngineNo = $request['EngineNo'];
                $ChassisNo = $request['ChassisNo'];
                $Cylinder = $request['Cylinder'];
                $SecondaryKey = $request['SecondaryKey'];
                $InventoryDate = $request['InventoryDate'];
                $InventoryAmount = $request['InventoryAmount'];
                $AssetCondition = $request['AssetCondition'];
                $ProposedAmount = $request['ProposedAmount'];
                $Proposed_Pickup_Date = $request['Proposed_Pickup_Date'];
                $Partner_InCharge = $request['Partner_InCharge'];
                $Partner_InCharge_MobilePhoneNo = $request['Partner_InCharge_MobilePhoneNo'];
                $OrderDate = $request['OrderDate'];
                $Notes = $request['Notes'];
                $OrderBy = "API";
                $Status = "NEW";
                $SentDate = $request['SentDate'];


                DB::Insert("INSERT INTO `supplychain`.`Order` (`PartnerID`,`PickupLocation`,`AgreementNo`, `AssetCode`, `AssetDescription`, `DocumentNo`, `TaxNoticed_No`, `LastAnnual_TaxNoticed_Billed`, `5Year_ExpiredDate_TaxNotice`,`AssetCategory`, `ManufacturingYear`, `LicensePlate`, `Colour`, `EngineNo`, `ChassisNo`, `Cylinder`, `SecondaryKey`, `InventoryDate`, `InventoryAmount`, `AssetCondition`, `ProposedAmount`,`Proposed_Pickup_Date`, `Partner_InCharge`, `Partner_InCharge_MobilePhoneNo`, `OrderDate`, `Notes`, `SentDate`, `OrderBy`, `Status`)
                            VALUES ('$PartnerID', '$PickupLocation','$AgreementNo', '$AssetCode', '$AssetDescription', '$DocumentNo', '$TaxNoticed_No', '$LastAnnual_TaxNoticed_Billed','$Year_ExpiredDate_TaxNotice', '$AssetCategory','$ManufacturingYear', '$LicensePlate', '$Colour', '$EngineNo', '$ChassisNo', '$Cylinder', '$SecondaryKey', '$InventoryDate', '$InventoryAmount', '$AssetCondition', '$ProposedAmount', '$Proposed_Pickup_Date','$Partner_InCharge', '$Partner_InCharge_MobilePhoneNo','$OrderDate','$Notes','$SentDate','$OrderBy','$Status');");
                // DB::connection('mysql2')->insert("insert into steam.bfireposses_json (json,status,Type,Created_at,Updated_at) value ('$json','success','Reposses','$date','$date')");
                return [
                    'header' => [
                        'status' => 200,
                        'message' => 'Data Berhasil diupload'
                    ],
                    'data' =>
                    [
                        [
                            'AgreementNo'       => $request['AgreementNo'],
                        ]
                    ]
                ];
            } else {
                // DB::connection('mysql2')->insert("insert into steam.bfireposses_json (json,status,Type,Created_at,Updated_at) value ('$json','failed','Reposses','$date','$date')");
                return [
                    'header' => [
                        'status' => 401,
                        'message' => 'Data sudah tersedia'
                    ],
                    'data' => []
                ];
            }
        } catch (\Exception $e) {
            // DB::connection('mysql2')->insert("insert into steam.bfireposses_json (json,status,Type,Created_at,Updated_at) value ('$json','failed','Reposses','$date','$date')");
            return [
                'header' => [
                    'status' => 401,
                    'message' => 'Data gagal di upload'
                ],
                'msg' => $e->getMessage(),
                'data' => []
            ];
        }
    }

    public function manualorder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|file|mimes:xlx,xlsx'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['error' => $validator->errors()->all(), 'isValid' => false, 'code' => '422'],
                422
            );
        }

        $path = $request->file('import_file');
        Excel::import(new OrderImport, $path);

        return response()->json(
            ['Message' => 'Upload Successfully', 'isValid' => true, 'code' => '200'],
            200
        );
        // return response()->json(['Message' => 'Upload Successfully'], 200);
    }

    public function trackingorder()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];

        $cektracking = DB::Select("SELECT id,AgreementNo,AssetDescription,OrderDate,LicensePlate,OrderBy,Status FROM supplychain.Order where PartnerID = '$PartnerID'");
        return response()->json(
            ['isValid' => true, 'code' => '200', 'data' => $cektracking, 'message' => 'success'],
            200
        );
    }

    public function trackingorderdetail()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $id        = $request['id'];

        $detailtracking = DB::Select("SELECT * FROM supplychain.Order where id = '$id'");
        return response()->json(
            ['isValid' => true, 'code' => '200', 'message' => 'success', 'data' => $detailtracking],
            200
        );
    }
    public function email()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        // $details = [
        //     'title' => 'Mail from ItSolutionStuff.com',
        //     'body' => 'This is for testing email using smtp'
        // ];

        // \Mail::to('kurniawan.andi09@gmail.com')->send(new \App\Mail\MyTestMail($details));

        // dd("Email is Sent.");

        // Mail::to('kurniawan.andi09@gmail.com')->send(new NotifyMail());
        $detail = DB::select("SELECT * from
                        steam.trSellingControlH
                        WHERE
                        id = '" . request()->id . "'")[0];
        DB::SELECT("SELECT @previouddate:=DATE_ADD(NOW(), INTERVAL - 1 DAY);

                        SELECT 
                            OrderNo,
                            LicensePlate,
                            AssetCode,
                            AssetDescription,
                            DocumentNo,
                            AssetCategory,
                            ManufacturingYear,
                            Colour,
                            EngineNo,
                            ChassisNo
                        FROM
                            supplychain.Order
                        WHERE
                            PartnerID = _PartnerID
                                AND OrderDate = @previouddate;");
        // $email = 'andi.kurniawan@sitama.co.id';
        $email = 'inventoryadm@bfi.co.id';
        Mail::send('emails.myTestMail', [
            'InvoiceNo' => $detail->InvoiceNo, 'InvoiceDate' => $detail->InvoiceDate,  'url' => env('APP_URL', null) . '/SellingControl/sellinggroup',
            'InvoiceAmount' => $detail->InvoiceAmount,  '_Status' => $detail->_Status,  'ChannelID' => $detail->ChannelID
        ], function ($message) use ($email, $detail) {
            $message->from('contact@sitama.co.id', 'STEAM Selling Control');
            $message->to($email);
            $message->subject('STEAM - Selling Control - ' . $detail->InvoiceNo);
        });

        if (Mail::failures()) {
            return response()->json(
                ['isValid' => false, 'code' => '400', 'message' => 'failed'],
                400
            );
        } else {
            return response()->json(
                ['isValid' => true, 'code' => '200', 'message' => 'success'],
                200
            );
        }

        // $nama       = 'andi kurniawan';
        // $pesan      = 'OKE';
        // $judul      = 'tesdoang';
        // $email      = 'kurniawan.andi09@gmail.com';

        // // try {
        // Mail::send('email', ['nama' => $nama, 'pesan' => $pesan, 'judul' => $judul, 'email' => $email], function ($message, $nama, $pesan, $judul, $email) use ($request) {
        //     $message->subject($judul);
        //     $message->from('donotreply@kiddy.com', 'Kiddy');
        //     $message->to($email);
        // });
        // // return back()->with('alert-success', 'Berhasil Kirim Email');
        // return response()->json(
        //     ['isValid' => true, 'code' => '200', 'message' => 'success'],
        //     200
        // );
        // } catch (Exception $e) {
        //     return response()->json(
        //         ['isValid' => false, 'code' => '422', 'message' => $e->getMessage()],
        //         422
        //     );
        // }
    }
    public function uploaddocument(request $request)
    {
        $validator = Validator::make($request->all(), [
            'PartnerID'         => 'required|string',
            'OrderID'           => 'required|string',
            'ImagesType'        => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID            = $request['PartnerID'];
        $OrderID          = $request['OrderID'];
        $ImagesType          = $request['ImagesType'];

        $date    = Carbon::now()->format('Y-m-d H:i:s');
        $Images              = $request->file('Images');
        $fileExtension          = $Images->getClientOriginalExtension();
        // dd($fileExtension);
        // $Photo              = str_replace('data:image/png;base64,', '', $Photo);
        $Images              = str_replace(' ', '+', $Images);

        $ImagesName          = time() . '_' . uniqid() . '.' . $fileExtension;
        Storage::disk('sftp')->put('/Vehiclo/Partner/' . $ImagesName, file_get_contents($Images));
        $Images_URL = 'https://file.sitama.co.id/storage/Vehiclo/Partner/' . $ImagesName;

        DB::Insert("INSERT INTO `supplychain`.`Order_Images`
        (`OrderID`,
        `PartnerID`,
        `ImagesName`,
        `ImagesType`,
        `Images_URL`,
        `SentDate`)
        VALUES
        ('$OrderID',
        '$PartnerID',
        '$ImagesName',
        '$ImagesType',
        '$Images_URL',
        '$date');");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'message' => 'upload document Succesfully',],
            200
        );
    }
    public function notification()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];

        $notif = DB::Select("SELECT 
            PartnerID, Message, SentDate
        FROM
            Partner_Notification
        where PartnerID = '$PartnerID'
        ORDER BY sentdate DESC
        LIMIT 10;");
        return response()->json(
            ['isValid' => true, 'code' => '200', 'data' => $notif, 'message' => 'success'],
            200
        );
    }
    public function OrderImages()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'id' => 'required',
            'Type' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $id        = $request['id'];
        $ImagesType = $request['Type'];

        $notif = DB::Select("SELECT * FROM supplychain.Order_Images where OrderID = '$id' and ImagesType = '$ImagesType'");
        return response()->json(
            ['isValid' => true, 'code' => '200', 'data' => $notif, 'message' => 'success'],
            200
        );
    }
}

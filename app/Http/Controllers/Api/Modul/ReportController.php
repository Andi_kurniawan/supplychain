<?php

namespace App\Http\Controllers\Api\Modul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;

class ReportController extends Controller
{
    public function ReportSelling()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];
        $Month        = $request['Month'];
        $Year        = $request['Year'];
        $data = DB::select("call supplychain.spc_rpt_selling('$PartnerID','$Month','$Year')");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function ReportInvoice()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID    = $request['PartnerID'];
        $Month        = $request['Month'];
        $Year         = $request['Year'];
        $data = DB::select("call supplychain.spc_rpt_invoices('$PartnerID','$Month','$Year')");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function ReportBuktiSetor()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID    = $request['PartnerID'];
        $Month        = $request['Month'];
        $Year         = $request['Year'];
        $data = DB::select("call supplychain.spc_rpt_BuktiSetor('$PartnerID','$Month','$Year')");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }
}

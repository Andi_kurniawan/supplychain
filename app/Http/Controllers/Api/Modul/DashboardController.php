<?php

namespace App\Http\Controllers\Api\Modul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;

class DashboardController extends Controller
{
    public function summary()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'ISMTD' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];
        $ISMTD        = $request['ISMTD'];
        $Month        = $request['Month'];
        $Year        = $request['Year'];

        if ($ISMTD == '1') {
            //topbrand
            $topbrand = DB::select("SELECT 
                @Brand:=Brand as Brand, @SellingBrand:=SUM(SellingAmount) as SellingAmount
            FROM
                supplychain.SellingTransactionResults
            WHERE
                PartnerID = '$PartnerID'
                    AND MONTH(SellingDate) = '$Month'
                    AND YEAR(SellingDate) = '$Year'
            GROUP BY Brand
            ORDER BY SUM(SellingAmount) DESC
            LIMIT 1;")[0];
            // Pricing Growth
            $pricinggrowth = DB::Select("SELECT 
                @PricingGrowth:=AVG((SellingAmount - BottomPrices) / SellingAmount) as pricinggrowth
            FROM
                supplychain.SellingTransactionResults
            WHERE
                PartnerID = '$PartnerID'
                    AND MONTH(SellingDate) = '$Month'
                    AND YEAR(SellingDate) = '$Year';")[0];
            // Success Rate
            $SuccessRate = DB::Select("SELECT 
                @SuccessRate:=SUM(SellingAmount) * 100.00 / SUM(BottomPrices) as successrate
            FROM
                supplychain.SellingTransactionResults
            WHERE
                PartnerID = '$PartnerID'
                    AND MONTH(SellingDate) = '$Month'
                    AND YEAR(SellingDate) = '$Year';")[0];

            // SoldAmount
            $soldamount = DB::Select("SELECT 
                @SoldAmount:=SUM(SellingAmount) as soldamount
            FROM
                supplychain.SellingTransactionResults
            WHERE
                PartnerID = '$PartnerID'
                    AND MONTH(SellingDate) = '$Month'
                    AND YEAR(SellingDate) = '$Year';")[0];

            // Sales Achievement
            $salesachievment = DB::Select("SELECT 
                @SalesAchievement:=SUM(IF(sell.OrderNo IS NULL, 0, 1)) * 100.00 / COUNT('') as salesachievment
            FROM
                supplychain.Order ord
                    LEFT JOIN
                supplychain.SellingTransactionResults sell ON ord.OrderNo = sell.OrderNo
            WHERE
                ord.PartnerID = '$PartnerID'
                    AND MONTH(ord.OrderDate) = '$Month'
                    AND YEAR(ord.OrderDate) = '$Year';")[0];
        } else {
            //topbrand
            $topbrand = DB::select("SELECT 
                @Brand:=Brand as Brand, @SellingBrand:=SUM(SellingAmount) as SellingAmount
            FROM
                supplychain.SellingTransactionResults
            WHERE
                PartnerID = '$PartnerID'
                    -- AND MONTH(SellingDate) = _Month
                    AND YEAR(SellingDate) = '$Year'
            GROUP BY Brand
            ORDER BY SUM(SellingAmount) DESC
            LIMIT 1;")[0];

            // Pricing Growth
            $pricinggrowth = DB::Select("SELECT 
                @PricingGrowth:=AVG((SellingAmount - BottomPrices) / SellingAmount) as pricinggrowth
            FROM
                supplychain.SellingTransactionResults
            WHERE
                    PartnerID = '$PartnerID'
                        -- AND MONTH(SellingDate) = _Month
                        AND YEAR(SellingDate) = '$Year';")[0];

            $SuccessRate = DB::Select("SELECT 
            @SuccessRate:=SUM(SellingAmount) * 100.00 / SUM(BottomPrices) as successrate
            FROM
                supplychain.SellingTransactionResults
            WHERE
                    PartnerID = '$PartnerID'
                        -- AND MONTH(SellingDate) = _Month
                        AND YEAR(SellingDate) = '$Year';")[0];


            // SoldAmount
            $soldamount = DB::Select("SELECT 
                @SoldAmount:=SUM(SellingAmount) as soldamount
            FROM
                supplychain.SellingTransactionResults
            WHERE
                PartnerID = '$PartnerID'
                    -- AND MONTH(SellingDate) = _Month
                    AND YEAR(SellingDate) = '$Year';")[0];


            // Sales Achievement
            $salesachievment = DB::Select("SELECT 
                @SalesAchievement:=SUM(IF(sell.OrderNo IS NULL, 0, 1)) * 100.00 / COUNT('') as salesachievment
            FROM
                supplychain.Order ord
                    LEFT JOIN
                supplychain.SellingTransactionResults sell ON ord.OrderNo = sell.OrderNo
            WHERE
                ord.PartnerID = '$PartnerID'
                    AND YEAR(ord.OrderDate) = '$Year';")[0];
        }

        // $data = DB::Select("call supplychain.spc_dashboard_summary('$PartnerID', '$ISMTD', '$Month', '$Year');");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => ['TopBrand' => $topbrand->Brand, 'PricingGrowth' => $pricinggrowth->pricinggrowth, 'SuccessRate' => $SuccessRate->successrate, 'SoldAmount' => $soldamount->soldamount, 'SalesAchievment' => $salesachievment->salesachievment]],
            200
        );
    }

    public function unitdistribution()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'ISMTD' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];
        $ISMTD        = $request['ISMTD'];
        $Month        = $request['Month'];
        $Year        = $request['Year'];

        $data = DB::Select("call supplychain.sp_dasboard_unit_distribution('$PartnerID', '$ISMTD', '$Month', '$Year');");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function salestrend()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'ISMTD' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID    = $request['PartnerID'];
        $ISMTD        = $request['ISMTD'];
        $Month        = $request['Month'];
        $Year         = $request['Year'];

        $data = DB::Select("call supplychain.spc_dashboard_salestrend('$PartnerID', '$ISMTD', '$Month', '$Year');");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function margintrend()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'ISMTD' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];
        $ISMTD        = $request['ISMTD'];
        $Month        = $request['Month'];
        $Year        = $request['Year'];

        $data = DB::Select("call supplychain.spc_dashboard_margintrend('$PartnerID', '$ISMTD', '$Month', '$Year');");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function summaryorder()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'ISMTD' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];
        $ISMTD        = $request['ISMTD'];
        $Month        = $request['Month'];
        $Year        = $request['Year'];

        $data = DB::Select("call supplychain.spc_dashboard_order('$PartnerID', '$ISMTD', '$Month', '$Year');")[0];

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function recentorder()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];

        $data = DB::Select("call supplychain.sp_recent_order('$PartnerID');");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function recentselling()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];

        $data = DB::Select("call supplychain.sp_recent_selling('$PartnerID');");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function OrderProgress()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'ISMTD' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];
        $ISMTD        = $request['ISMTD'];
        $Month        = $request['Month'];
        $Year        = $request['Year'];

        $data = DB::Select("call supplychain.spc_order_progress('$PartnerID', '$ISMTD', '$Month', '$Year');")[0];

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function salestrend2()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'ISMTD' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID    = $request['PartnerID'];
        $ISMTD        = $request['ISMTD'];
        $Month        = $request['Month'];
        $Year         = $request['Year'];

        $data = DB::Select("call supplychain.spc_dashboard_salestrend('$PartnerID', '$ISMTD', '$Month', '$Year');");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function margintrend2()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'ISMTD' => 'required',
            'Month' => 'required',
            'Year' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];
        $ISMTD        = $request['ISMTD'];
        $Month        = $request['Month'];
        $Year        = $request['Year'];

        $data = DB::Select("call supplychain.spc_dashboard_margintrend('$PartnerID', '$ISMTD', '$Month', '$Year');");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }
}

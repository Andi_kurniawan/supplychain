<?php

namespace App\Http\Controllers\Api\Modul;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Validator;

class InvoiceController extends Controller
{
    public function invoice()
    {

        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];
        $data = DB::select("call supplychain.spc_view_Invoices('$PartnerID')");
        // $data = DB::Select("SELECT InvoiceNo, InvoiceDate, ProcessDate, SellingAmount, Status FROM supplychain.InvoiceH");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }
    

    public function reportinvoice()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'Invoice' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];
        $Invoice        = $request['Invoice'];

        $data = DB::select("Call spc_rpt_invoices ('$PartnerID','$Invoice');");
        // $data = DB::Select("SELECT InvoiceNo, InvoiceDate, ProcessDate, SellingAmount, Status FROM supplychain.InvoiceH");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }

    public function buktisetor()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        $validator = Validator::make($request, [
            'PartnerID' => 'required',
            'Invoice' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $PartnerID        = $request['PartnerID'];
        $Invoice        = $request['Invoice'];

        $data = DB::select("Call spc_rpt_BuktiSetor ('$PartnerID','$Invoice');");
        // $data = DB::Select("SELECT InvoiceNo, InvoiceDate, ProcessDate, SellingAmount, Status FROM supplychain.InvoiceH");

        return response()->json(
            ['isValid' => true, 'code' => '200', 'ResponseDescription' => 'Data tampil', 'Message' => $data],
            200
        );
    }
}

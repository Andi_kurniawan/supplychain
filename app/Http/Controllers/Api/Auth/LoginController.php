<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Auth;
use Cookie;
use Carbon\Carbon;
use App\Models\User;
use DB;
use App\Http\Controllers\ResponseFormatter;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8',
        ]);

        if ($validator->fails()) {

            return response()->json(
                ['error' => $validator->errors()->all(), 'authUser' => false, 'code' => '422'],
                422
            );
        }

        $credentials = request(['email', 'password']);
        // dd($credentials);
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            // dd($user);
            // $token =  $user->createToken('Personal Access Token')->accessToken;

            $tokenResult = $user->createToken($user->name);
            $token = $tokenResult->token;
            // $expires_at = Carbon::now()->addWeeks(1);
            // dd($expires_at);

            $minutes = 1440;
            if ($request->remember_me) {
                $token->expires_at = Carbon::now()->addWeeks(1);
                $menit = Carbon::now()->diff(new Carbon($token->expires_at));

                $minutes = $menit->days * 24 * 60;
                $minutes += $menit->h * 60;
                $minutes += $menit->i;
            }

            // dd($minutes);

            $token->save();

            $cookie = $this->getCookieDetails($tokenResult->accessToken, $minutes,);
            $detail = DB::select("SELECT * FROM supplychain.Partner where PartnerID = '$user->PartnerID'");
            return response()
                ->json([
                    'logged_in_user' => $user,
                    'detail'    => $detail,
                    'token' => $tokenResult->accessToken,
                    'expires_at' => $token->expires_at,
                    'authUser' => true,
                    'code' => '200'
                ], 200)
                ->cookie($cookie['name'], $cookie['value'], $cookie['minutes'], $cookie['path'], $cookie['domain'], $cookie['secure'], $cookie['httponly'], $cookie['samesite']);
        } else {

            return response()->json(

                ['error' => array('invalid-credentials'), 'authUser' => false, 'code' => '422'],
                422
            );
        }
    }

    private function getCookieDetails($token, $minutes)
    {
        return [
            'name' => '_token',
            'value' => $token,
            'minutes' => $minutes,
            'path' => null,
            'domain' => null,
            // 'secure' => true, // for production
            'secure' => null, // for localhost
            'httponly' => true,
            'samesite' => true,
        ];
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $cookie = Cookie::forget('_token');
        return response()->json([
            'message' => 'successful-logout'
        ])->withCookie($cookie);
    }

    public function cektoken()
    {
        return response()->json(['message' => 'success']);
    }

    public function checkEmailAndSendOtp()
    {
        // get content input
        $json = file_get_contents('php://input');

        // merubah json ke string
        $request = json_decode($json, true);

        $email          = $request['email'];
        $nomortelepon   = $request['nomortelepon'];

        if (!preg_match('/[^+0-9]/', trim($nomortelepon)))
        // cek apakah no hp mengandung karakter + dan 0-9
        {
            if (substr(trim($nomortelepon), 0, 3) == '+62')
            // cek apakah no hp karakter 1-3 adalah +62
            {
                $notelp = trim($nomortelepon);
            } elseif (substr(trim($nomortelepon), 0, 1) == '0')
            // cek apakah no hp karakter 1 adalah 0
            {
                $notelp = '+62' . substr(trim($nomortelepon), 1);
            }
            // fungsi trim() untuk menghilangan
            // spasi yang ada didepan/belakang
        } else {
            $notelp = $nomortelepon;
        }

        $data = DB::connection('mysql')->select("SELECT * FROM e_grosir.users WHERE email = '$email' AND NoHP = '$nomortelepon'");

        $otp = mt_rand(100000, 999999);

        if ($data) {
            try {
                PasswordReset::updateOrCreate(
                    ['email' => $email],
                    [
                        'email' => $email,
                        'token' => Hash::make($otp)
                    ]
                );

                //    send sms gateway
                $curltoken = curl_init();
                $data = array('Username' => env('GM_USER', ''), 'password' => env('GM_PASS', ''));
                curl_setopt($curltoken, CURLOPT_URL, 'https://smsgw.sitama.co.id/api/oauth/token');
                curl_setopt($curltoken, CURLOPT_POST, 1);
                curl_setopt($curltoken, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curltoken, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curltoken, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($curltoken);
                curl_close($curltoken);

                $token = json_decode($result, true)['access_token'];

                // dd($token);

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://smsgw.sitama.co.id/api/SMS/smssitama",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_POSTFIELDS => "{\n    \"notelp\": \"$notelp\",\n    \"textsms\": \"SupplyChain%0AYour OTP Code is $otp\",\n    \"desc\": \"Grosir&Mobil\"\n}",
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer " . $token,
                        "Content-Type: application/json"
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);

                // Send Mail
                Mail::send('email.EmailOtp', ['Nama' => "test", 'Url' => "test", 'Otp' => $otp], function ($message) use ($request) {
                    $message->from('contact@sitama.co.id', 'Grosir Mobil');
                    $message->to($request['email'], 123123123 . ' ' . "testing");
                    $message->subject('Kode OTP Grosir Mobil');
                });
                // End Mail

                // dd($passwordReset);

                return ResponseFormatter::success(null, 'Kode OTP sukses dikirim!!');
            } catch (\Throwable $e) {
                return ResponseFormatter::error(null, 'Kode OTP gagal dikirim!!', 200);
            }

            return ResponseFormatter::success($data, 'Data ditemukan!!');
        } else {
            return ResponseFormatter::error(null, 'Data tidak ditemukan!!', 200);
        }
    }

    public function checkValidationOTP()
    {
        // get content input
        $json = file_get_contents('php://input');

        // merubah json ke string
        $request = json_decode($json, true);

        $email  = $request['email'];
        $otp    = $request['otp'];

        $emailCheck = PasswordReset::where('email', $email)->exists();

        if ($emailCheck) {
            $emailData = PasswordReset::where('email', $email)->first();

            if (password_verify($otp, $emailData['token'])) {
                $user = User::where('email', $email)->first();

                $tokenResult = $user->createToken($user->Nama_Lengkap);
                $token = $tokenResult->token;

                $minutes = 1440;

                $token->save();

                $cookie = $this->getCookieDetails($tokenResult->accessToken, $minutes);
                return response()
                    ->json([
                        'status' => 'success',
                        'message' => 'Kode Otp anda cocok!!',
                        'logged_in_user' => $user,
                        'token' => $tokenResult->accessToken,
                        'authUser' => true
                    ], 200)
                    ->cookie($cookie['name'], $cookie['value'], $cookie['minutes'], $cookie['path'], $cookie['domain'], $cookie['secure'], $cookie['httponly'], $cookie['samesite']);
            } else {
                return ResponseFormatter::error(null, 'Kode OTP tidak cocok!!', 200);
            }
        } else {
            return ResponseFormatter::error(null, 'Email tidak ditemukan!!', 200);
        }
    }

    public function changePasswordForgot()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $email          = $request['email'];
        $newpassword    = $request['newpassword'];
        $user           = User::where('email', $email)->first();

        try {
            $password = User::find($user['id']);
            $password->password = Hash::make($newpassword);
            $password->save();
            return ResponseFormatter::success(null, 'Password berhasil diubah!!');
        } catch (\Throwable $e) {
            return ResponseFormatter::error(null, 'Password gagal diubah!!', 200);
        }
    }

    public function changePassword()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $validator = Validator::make($request, [
            'newpassword' => 'required|string|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => $validator->errors(),],
                422
            );
        }

        $id_user            = $request['id_user'];
        $old_password       = $request['old_password'];
        $newpassword        = $request['newpassword'];
        $confirmnewpassword = $request['confirmnewpassword'];

        $cekLoginID = DB::select("SELECT * FROM supplychain.users where id = '$id_user'");

        // Jika Cek Login tidak kosong
        if (!empty($cekLoginID)) {
            $oldpassworddatabasebyuser = DB::select("SELECT * FROM supplychain.users where id = '$id_user'");
            foreach ($oldpassworddatabasebyuser as $old) {
                if (password_verify($old_password, $old->password)) {
                    // Jika Password Sama
                    if ($newpassword === $confirmnewpassword) {
                        // Action Hash Password
                        $hashnewpassword = Hash::make($newpassword);
                        try {
                            // Update Password yang terhash
                            DB::update("UPDATE supplychain.users SET `password` = '$hashnewpassword' WHERE (`id` = '$id_user');");
                            // Return hasil

                            return response()->json(
                                ['isValid' => true, 'code' => '200', 'message' => 'Succesfully Change Password',],
                                200
                            );
                        } catch (\Throwable $th) {
                            // Return Hasil

                            return response()->json(
                                ['isValid' => false, 'code' => '422', 'message' => 'Gagal merubah password',],
                                422
                            );
                        }
                    } else {
                        // Return Hasil

                        return response()->json(
                            ['isValid' => false, 'code' => '422', 'message' => 'Password and Confirm Password Not Same',],
                            422
                        );
                    }
                } else {
                    return response()->json(
                        ['isValid' => false, 'code' => '422', 'message' => 'Old Password is Wrong',],
                        422
                    );
                }
            }
        } else {
            return response()->json(
                ['isValid' => false, 'code' => '422', 'message' => 'Data not found',],
                422
            );
        }
    }
}

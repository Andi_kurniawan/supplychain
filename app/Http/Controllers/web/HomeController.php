<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view("home");
    }
    public function order()
    {
        return view("order");
    }
    public function login()
    {
        return view("login");
    }
    public function uploadorder()
    {
        return view("uploadorder");
    }
}

<?php

namespace App\Imports;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use DB;

class OrderImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $collection
     */

    public function collection(Collection $collection)
    {
        HeadingRowFormatter::default('none');


        foreach ($collection as $r) {
            $year  = Carbon::now()->format('y');
            $query = DB::SELECT("SELECT max(id)+1 as maxid FROM supplychain.Order");
            // dd($query);
            foreach ($query as $i) {
                if ($i->maxid == null) {
                    $mx = 1;
                } else {
                    $mx = $i->maxid;
                }
            }

            $OrderNo = $year . $r['partnerid'] . sprintf("%06s", $mx);

            Order::create([
                'OrderNo'  => $OrderNo,
                'PartnerID' => $r['partnerid'],
                'PickupLocation' => $r['pickuplocation'],
                'AgreementNo' => $r['agreementno'],
                // 'AssetID' => $r['assetid'],
                // 'Brand' => explode(',', $r['assetcode'], 1),
                'AssetCode' => $r['assetcode'],
                'AssetDescription' => $r['assetdescription'],
                'DocumentNo' => $r['documentno'],
                'TaxNoticed_No' => $r['taxnoticed_no'],
                'LastAnnual_TaxNoticed_Billed' => Carbon::parse($r['lastannual_taxnoticed_billed']),
                'Year_ExpiredDate_TaxNotice' => Carbon::parse($r['5year_expireddate_taxnotice']),
                'AssetCategory' => $r['assetcategory'],
                'ManufacturingYear' => $r['manufacturingyear'],
                'LicensePlate' => $r['licenseplate'],
                'Colour' => $r['colour'],
                'EngineNo' => $r['engineno'],
                'ChassisNo' => $r['chassisno'],
                'Cylinder' => $r['cylinder'],
                'SecondaryKey' => $r['secondarykey'],
                'InventoryDate' => Carbon::parse($r['inventorydate']),
                'InventoryAmount' => $r['inventoryamount'],
                'AssetCondition' => $r['assetcondition'],
                'ProposedAmount' => $r['proposedamount'],
                'Proposed_Pickup_Date' => Carbon::parse($r['proposed_pickup_date']),
                'Partner_InCharge' => $r['partner_incharge'],
                'Partner_InCharge_MobilePhoneNo' => $r['partner_incharge_mobilephoneno'],
                'OrderDate' => Carbon::now(),
                'Notes' => $r['notes'],
                // 'SentDate' => Carbon::parse($r['sentdate']),
                'SentDate' => Carbon::now(),
                'OrderBy' => 'Excel',
                'Status'  => 'New Order',
                'Created_at' => Carbon::now(),
                'Updated_at' => Carbon::now()
            ]);
            DB::update("UPDATE supplychain.`Order`
            SET Brand = Replace(SUBSTRING_INDEX(AssetCode, '.', 1),'MTR','')
            WHERE Brand is null and id>0;");
        }
    }
}
